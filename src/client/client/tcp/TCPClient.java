package client.client.tcp;

import java.io.*;
import java.net.Socket;

public class TCPClient implements Closeable {
    private Socket clientSocket;
    private BufferedReader input;
    private BufferedWriter output;

    public void initializeConnection(String ipv4Addr, int port) throws IOException {
        clientSocket = new Socket(ipv4Addr, port);
        input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
    }

    public String sendMessage(String message) throws IOException {
        output.write(message);
        output.flush();
        return input.readLine();
    }

    @Override
    public void close() throws IOException {
        output.close();
        input.close();
        clientSocket.close();
    }
}
