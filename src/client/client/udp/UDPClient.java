package client.client.udp;

import java.io.*;
import java.net.*;

public class UDPClient implements Closeable {
    private final DatagramSocket clientSocket;
    private final InetAddress address;
    private final int serverPort;

    public UDPClient(String serverHost, int serverPort) throws SocketException, UnknownHostException {
        clientSocket = new DatagramSocket();
        address = InetAddress.getByName(serverHost);
        this.serverPort = serverPort;
    }

    public String sendMessage(String message) throws IOException {
        byte[] buf = message.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, serverPort);
        clientSocket.send(packet);
        packet = new DatagramPacket(buf, buf.length);
        clientSocket.receive(packet);
        return new String(packet.getData(), 0, packet.getLength());
    }

    public String receiveMessage() throws IOException {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivedPacket = new DatagramPacket(receiveData, receiveData.length);
        clientSocket.receive(receivedPacket);
        return new String(receivedPacket.getData(), 0, receivedPacket.getLength());
    }

    @Override
    public void close() {
        clientSocket.close();
    }
}
