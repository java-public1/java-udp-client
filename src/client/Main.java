package client;

import client.client.tcp.TCPClient;
import client.client.udp.UDPClient;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (UDPClient UDPClient = new UDPClient("localhost", 8080); TCPClient tcpClient = new TCPClient()) {
            tcpClient.initializeConnection("localhost", 8080);
            tcpClient.sendMessage("flag"); //inicjalizacja polaczenia z serwerem tcp
            tcpClient.sendMessage("192.168.0.1:8082");

            String r1 = UDPClient.receiveMessage();
            System.out.println(r1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
